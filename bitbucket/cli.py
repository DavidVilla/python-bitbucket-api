# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import argparse
import logging

from .api import Config, BUCKETCONFIG


class Parser(argparse.ArgumentParser):
    def __init__(self, *args, **kargs):
        self.preload = []

        if 'logger' in kargs:
            self.logger = kargs['logger']
            del kargs['logger']
        else:
            self.logger = logging

        kargs['description'] = 'Bitbucket CLI'
        kargs['prog'] = 'bucket'
#        kargs['argument_default'] = argparse.SUPPRESS
        argparse.ArgumentParser.__init__(self, *args, **kargs)

        self.add_argument('-a', '--account', dest='account',
                          help='Account in the form user[:pass]')

        self.add_argument('-c', '--config',
                          help='Read config from specified file')

        self.add_argument('-v', '--verbose', action='count',
                          help='Be verbose')

        self.commands = self.add_subparsers(dest='command', metavar='command',
                                            help='commands help')
        self.commands._parser_class = argparse.ArgumentParser

        self.add_command_config()
        self.add_command_list()
        self.add_command_status()
        self.add_command_create()
        self.add_command_set()
        self.add_command_delete()
        self.add_command_clone()
        self.add_command_sync()
        self.add_command_repo_groups()
        self.add_command_group()
        self.add_command_ssh()
        self.add_command_issue()

    def add_parser(self, *args, **kargs):
        kargs['argument_default'] = argparse.SUPPRESS
        return self.commands.add_parser(*args, **kargs)

    def add_command_config(self):
        self.add_parser('show-config',
                        help='Show current config')

    def add_command_ssh(self):
        parser = self.add_parser('ssh-ls',
                                 help='List your account SSH keys')

        parser = self.add_parser('ssh-add',
                                 help='Upload a SSH key to your account')
        parser.add_argument('keylabel')
        parser.add_argument('keyfile', type=argparse.FileType('r'))

        parser = self.add_parser('ssh-del',
                                 help='Delete a SSH key by label')
        parser.add_argument('keylabel',
                            help='key label. Run ssh-ls')

    def add_command_list(self):
        parser = self.add_parser('ls', help='List repos')
        parser.add_argument(
            'owner', nargs='?',
            help='Bitbucket repo owner [default:authenticated user]')

        parser.add_argument('-s', '--show-scm',
                            dest='ls.show_scm',
                            action='store_true',
                            help='show SCM')
        parser.add_argument('-d', '--show-desc',
                            dest='ls.show_desc',
                            action='store_true',
                            help='show repo description')
        parser.add_argument('-a', '--show-access',
                            dest='ls.show_access',
                            action='store_true',
                            help='show repo access: private/public')
        parser.add_argument('--public', dest='ls.access',
                            action='store_const', const='public',
                            help='list public repos only')
        parser.add_argument('--private', dest='ls.access',
                            action='store_const', const='private',
                            help='list private repos only')
        parser.add_argument('--hg', dest='ls.scm',
                            action='store_const', const='hg',
                            help='list mercurial repos only')
        parser.add_argument('--git', dest='ls.scm',
                            action='store_const', const='git',
                            help='list git repos only')

    def add_command_status(self):
        parser = self.add_parser('st', help='Show repo status')
        parser.add_argument('reponames', metavar='repo-names', nargs='*',
                            default=None, help='repos')
        parser.add_argument('--already', action='store_true',
                            dest='st.already',
                            help='check all local directory repos')
        parser.add_argument('--remote', action='store_true',
                            dest='st.remote',
                            help='check remote origin')

    def add_command_create(self):
        parser = self.add_parser('create',
                                 help='Create remote repo')
        parser.add_argument('reponame', metavar='[owner]/repository',
                            help='name of the new repo')
        parser.add_argument('--public', dest='create.private',
                            action='store_false',
                            help='make this repo public')
        parser.add_argument('--private', dest='create.private',
                            action='store_true',
                            help='make this repo private')
        parser.add_argument('--hg', dest='create.scm',
                            action='store_const', const='hg',
                            help='create a mercurial repo')
        parser.add_argument('--git', dest='create.scm',
                            action='store_const', const='git',
                            help='create a git repo')
        parser.add_argument('--desc', metavar='TEXT',
                            dest='create.desc',
                            help='repository description')
        parser.add_argument('--clone', dest='create.clone',
                            action='store_true',
                            help='clone newly created repo')

    #FIXME
    def add_command_set(self):
        parser = self.add_parser('set',
                                 help='Change repo config')
        parser.add_argument('reponame', metavar='name',
                            help='name of the new repo')
        parser.add_argument('--public', dest='set.private',
                            action='store_false',
                            help='make this repo public')
        parser.add_argument('--private', dest='set.private',
                            action='store_true',
                            help='make this repo private')

    def add_command_delete(self):
        parser = self.add_parser('del',
                                 help='Delete upstream remote repo (at bitbucket!!)')
        parser.add_argument('reponame', metavar='name',
                            help='name of the repo to delete')

    def add_command_clone(self):
        parser = self.add_parser('clone',
                                 help="Clone remote repo")
        self.add_proto_args(parser, 'clone.proto')

        parser.add_argument('reponame', metavar='[owner]/repository',
                            help='name of the repo to clone')
        parser.add_argument('--destdir', metavar='dirname',
                            dest="clone.destdir",
                            help='where to clone')

    def add_command_sync(self):
        parser = self.add_parser('sync',
                                 help='Sync (clone/update) repos of same owner')
        self.add_proto_args(parser, 'clone.proto')

        parser.add_argument('reponames', metavar='repo-names', nargs='*',
                            default=None,
                            help='repos to sync')
        # FIXME: same of config clone??
        parser.add_argument('--destdir', metavar='dirname',
                            dest="clone.destdir",
                            help='where to store repos [default:$(cwd)]')
        parser.add_argument('--all', action='store_true',
                            dest='sync.all',
                            help='clone/update all repos of given owner')
        parser.add_argument('--already', action='store_true',
                            dest='sync.already',
                            help='update local directory repos of given owner')
        parser.add_argument('-o', '--owner', metavar='OWNER',
                            dest='sync.owner',
                            help='repo owner account name [default:authenticated user]')

    def add_proto_args(self, parser, dest):
        parser.add_argument('--ssh', dest=dest,
                            action='store_const', const='ssh',
                            help='use SSH to clone')
        parser.add_argument('--http', dest=dest,
                            action='store_const', const='http',
                            help='use HTTP to clone')

    def add_command_issue(self):
        parser = self.add_parser('issue-ls',
                                 help='List this repo issues')
        parser.add_argument('reponame', metavar='[owner]/repository',
                            help='name of the repo to clone')

    def add_command_repo_groups(self):
        parser = self.add_parser('group-ls',
                                 help='list granted groups for repository')
        parser.add_argument('reponame', metavar='[owner]/repository',
                            help='name of the repo')

        parser = self.add_parser('group-grant',
                                 help='set group privileges for repository')
        parser.add_argument('reponame', metavar='[owner]/repository',
                            help='name of the repo')
        parser.add_argument('group', metavar='[owner]/group',
                            help='group')
        parser.add_argument('privilege',
                            help='read|write|admin')

    def add_command_group(self):
        parser = self.add_parser('group-repos',
                                 help='list repos that grant acces to given group')
        parser.add_argument('owner', metavar="account")
        parser.add_argument('group', metavar='[owner]/group')

    def get_config_file(self, argv):
        return self.parse_args(argv).config

    def parse(self, argv=None, config=None):

        if isinstance(argv, str):
            argv = argv.split()

        if argv is None:
            argv = sys.argv[1:]

        if config is None:
            config = Config()
            config_file = self.get_config_file(argv) or BUCKETCONFIG
            self.logger.info("loading config from: %s", config_file)
            config.load(config_file)

        self.parse_args(argv, namespace=config)

        config.resolve()
        return config
