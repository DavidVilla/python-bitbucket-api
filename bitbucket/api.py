# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import sys
import time
import logging
import io

from functools import total_ordering

import getpass

requests_log = logging.getLogger("urllib3")
requests_log.setLevel(logging.WARNING)
requests_log.propagate = False

import configobj
import validate
import json
import requests

from commodity.os_ import SubProcess, LineStreamPrefixer
from commodity.os_ import resolve_path, FileTee
from commodity.pattern import MetaBunch
from commodity.str_ import Printable

import bitbucket.exceptions as exc

BB_HTTP_BASE = 'https://{credentials}bitbucket.org/{owner}/{repo}/'
BB_SSH_BASE = 'ssh://hg@bitbucket.org/{owner}/{repo}/'
BB_API = 'https://api.bitbucket.org/1.0/{path}'

USERDIR = os.environ['HOME']
BUCKETCONFIG = os.path.join(USERDIR, '.bucket')
BUCKETSPECS = resolve_path('config.spec', ['.', '/usr/lib/bucket'])[0]


def get_password(username):
    return getpass.getpass("{0}'s password: ".format(username))


class Credentials(Printable):
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __unicode__(self):
        return u"<Credentials %s>" % self.username


class OwnedResource(object):
    def __init__(self, path, default_owner=None):
        if not '/' in path:
            path = '/' + path

        self.owner, self.name = path.split('/')
        self.owner = self.owner or default_owner

    def __str__(self):
        return "{0}/{1}".format(self.owner, self.name)


class Account(Printable):
    def __init__(self, accounts, account, get_pass_cb=get_password):
        self.accounts = accounts
        self.get_pass = get_pass_cb
        self.username = None
        self.password = None
        self.load(account)

    def load(self, account):
        # explicit account
        if account and ':' in account:
            return self._get_account(account)

        if account is None:
            account = 'default'
            if not self._has_account(account):
                return self._get_account(None)

        if not self._has_account(account):
            return self._get_account(account)

        return self._get_account_by_label(account)

    def _get_account_by_label(self, label):
        user_pass = self.accounts[label]
        return self._get_account(user_pass)

    def _get_account(self, user_and_pass):
        if user_and_pass is None:
            return None

        try:
            username, password = user_and_pass.split(':')
        except ValueError:
            username, password = user_and_pass, None

        if not password:
            password = self.get_pass(username)

        self.username = username
        self.password = password

    def _has_account(self, label):
        if label is None:
            return False

        return label in self.accounts

    def __nonzero__(self):
        return self.username is not None

    def __unicode__(self):
        return u"<Account %s:%s>" % (self.username, self.password)


class Config(MetaBunch):
    def __init__(self):
        cobj = configobj.ConfigObj(configspec=BUCKETSPECS)
        cobj.validate(validate.Validator(), copy=True)
        MetaBunch.__init__(self, cobj)
        self.accounts = {}

    def load(self, data):
        if data is None:
            return

        self.dct.merge(configobj.ConfigObj(data))

    def resolve(self):
        self.account = Account(self.accounts, self.account)
        self.clone.destdir = os.path.expanduser(self.clone.destdir)

    def __str__(self):
        retval = '<Config\n'
        for key, item in self.items():
            retval += "  %s: %s\n" % (key, repr(item))
        return retval


def exec_command(cmd, verbose=False):
    out = io.BytesIO()
    err = io.BytesIO()

    outs = [out]
    if verbose:
        outs.append(LineStreamPrefixer(sys.stdout, '| '))

    ps = SubProcess(cmd, stdout=FileTee(*outs), stderr=err, shell=True)
    ps.wait()

    if ps.returncode:
        print err.getvalue()
        raise exc.CommandError(cmd)

    return out, err


def reply2json(req, ok=None, errors=None):

    def raise_error(error):
        e = error[0]
        args = error[1:]
        raise e(*args)

    ok = ok or 200
    errors = errors or {}

    if req.status_code != ok:
        raise_error(errors[req.status_code])

    if not req.content:
        return None

    return json.loads(req.content)


def bb_api_uri(path):
    uri = BB_API.format(path=path)
    return uri


def bb_http_uri(**kargs):
    uri = BB_HTTP_BASE.format(**kargs)
    return uri


def bb_ssh_uri(**kargs):
    uri = BB_SSH_BASE.format(**kargs)
    return uri


class Session(object):
    def __init__(self, credentials=None, logger=None, verbose=False):
        self.credentials = credentials
        self.timeout = 10
        self._http_session = None
        self.logger = logger or logging.getLogger('bitbucket')

        self.user = None
        if self.credentials is not None:
            self.user = self.credentials.username

        self.verbose = verbose

    def auth(self):
        if not self.credentials:
            return None

        return (self.credentials.username, self.credentials.password)

    @property
    def http_session(self):
        if self._http_session is not None:
            return self._http_session

        self._http_session = requests.Session()
        self._http_session.auth = self.auth()
        self._http_session.timeout = self.timeout

        if self.logger.level == logging.DEBUG:
            pass
#            self._http_session.config['verbose'] = sys.stderr

        if self.credentials:
            self.test_auth()

        return self._http_session

    def test_auth(self):
        errors = {
            400: [exc.InvalidOrAlreadyRegisteredSSHkey],
            401: [exc.InvalidCredentials]}

        self.http_get('ssh-keys', errors=errors)

    def request(self, method, path, data=None, ok=None, errors=None):
        while 1:
            try:
                reply = self.http_session.request(method, bb_api_uri(path), data=data)
                retval = reply2json(reply, ok=ok, errors=errors)
                break

            except exc.ServiceUnavailable:
                time.sleep(1)

            except requests.exceptions.ConnectionError, e:
                self.logger.error(e)
                sys.exit(1)

        return retval

    def http_get(self, path, **kargs):
        return self.request('get', path, **kargs)

    def http_put(self, path, **kargs):
        return self.request('put', path, **kargs)

    def http_post(self, path, **kargs):
        return self.request('post', path, **kargs)

    def http_delete(self, path, **kargs):
        return self.request('delete', path, **kargs)

    def create_ssh_key_manager(self):
        return SshKeyManager(self)

    def create_repo_manager(self, **kargs):
        return RepoManager(self, **kargs)

    def get_group(self, group_uri):
        return Group(self, group_uri)

    def __repr__(self):
        return "<Session credentials:%s>" % self.credentials


class SshKeyManager:
    def __init__(self, session):
        self.session = session

    def list_keys(self):
        return self.session.http_get('ssh-keys')

    def list_key_labels(self):
        return [x['label'] for x in self.list_keys()]

    def list_key_summaries(self):
        retval = []

        template = "pk: {pk:>8} |  label: {label:<12} |  {key_summary}"
        for key in self.list_keys():
            retval.append(template.format(label=key['label'],
                                          pk=key['pk'],
                                          key_summary=self._key_summary(key['key'])))
        return retval

    def _key_summary(self, keycontent):
        fields = keycontent.split()
        proto = seq = host = ''
        try:
            proto = fields[0]
            seq = fields[1]
            host = fields[2]
        except IndexError:
            pass

        return "{0} {1}...{2} {3}".format(proto, seq[:12], seq[-12:], host)

    def add_key(self, label, keyfile):
        errors = {
            400: [exc.InvalidOrAlreadyRegisteredSSHkey],
            401: [exc.InvalidCredentials]}

        retval = self.session.http_post(
            'ssh-keys', data=dict(label=label, key=keyfile.read()), errors=errors)
        return retval['pk']

    def delete_key_by_id(self, pk):
        errors = {
            404: [exc.NoSuchKey, pk]}

        self.session.http_delete('ssh-keys/%s' % pk, ok=204, errors=errors)

    def find_key_by_label(self, label):
        for key in self.list_keys():
            if key['label'] == label:
                return key['pk']

        return None

    def delete_key_by_label(self, label):
        pk = self.find_key_by_label(label)
        if pk is None:
            raise exc.NoSuchKey(label)

        self.delete_key_by_id(pk)


class FieldManager(object):
    def __init__(self, manager, name):
        self.manager = manager
        self.name = name

    @property
    def _repo_fields(self):
        return self.manager.get_repo_fields(self.name)

    def __getitem__(self, key):
        return self._repo_fields[key]

    def __setitem__(self, key, value):
        self.manager.set_repo_field(self.name, key, value)

    def keys(self):
        return self._repo_fields.keys()

    def copy(self):
        return self._repo_fields.copy()

    def __repr__(self):
        return str(self._repo_fields)


class Issue(Printable):
    def __init__(self, data):
        self.data = data

    def __unicode__(self):
        return u'%4s: %s' % (self.data['local_id'], self.data['title'])


class IssueManager(object):
    def __init__(self, repo):
        self.repo = repo
        self.session = repo.session

    def print_list(self):
        issues = self.session.http_get('repositories/%s/%s/issues' % (
                self.repo.owner, self.repo.name))

        print "-- issue list:"
        for data in issues['issues']:
            print Issue(data)


@total_ordering
class Repo(object):
    def __init__(self, name, repo_manager):
        self.name = name
        self.repo_manager = repo_manager
        self.session = repo_manager.session
#        self.owner = repo_manager.owner
        self._uri = None
        self.verbose = repo_manager.session.verbose

        self.uri_factories = {
            'http': self.bb_http_uri,
            'ssh': self.bb_ssh_uri}

        self.fields = FieldManager(repo_manager, name)
        self.owner = self.fields['owner']
        self.logger = self.repo_manager.logger

    @property
    def uri(self):
        if self._uri is None:
            proto = self.repo_manager.proto
            return self.uri_factories[proto]()

        return self._uri

    # FIXME: property
    def bb_http_uri(self):
        kargs = dict(credentials='', owner=self.owner, repo=self.name)
        if self.fields['is_private']:
            kargs['credentials'] = '{0}:{1}@'.format(*self.session.auth())

        return bb_http_uri(**kargs)

    # FIXME: property
    def bb_ssh_uri(self):
        kargs = dict(owner=self.owner, repo=self.name)
        return bb_ssh_uri(**kargs)

    def assure_local_copy(self, path):
        if not self.have_local_copy(path):
            raise exc.NotLocalCopy(self.name)

    def have_local_copy(self, path):
        """
        have_local_copy(str) -> boolean

        Return True if there is a repository in 'path'
        """
        raise NotImplemented

    def clone(self, path):
        raise NotImplementedError

    def update(self, path):
        raise NotImplementedError

    def sync(self, destdir):
        assert os.path.isdir(destdir), destdir
        if not self.have_local_copy(destdir):
            self.clone(destdir)
        else:
            self.update(destdir)

    def get_issue_manager(self):
        return IssueManager(self)

    def get_groups(self):
        groups = self.session.http_get('group-privileges/%s/%s' % (
            self.owner, self.name))
        return [MetaBunch(g) for g in groups]

    def grant_access_to_group(self, group, privilege):
        assert privilege in ['read', 'write', 'admin', 'none']
        uri = 'group-privileges/%s/%s/%s' % (self.owner, self.name, group)

        if privilege == 'none':
            self.session.http_delete(uri, ok=204)
        else:
            self.session.http_put(uri, data=privilege)

    def __eq__(self, other):
        return unicode(self) == unicode(other)

    def __lt__(self, other):
        return unicode(self) < unicode(other)

    def __unicode__(self):
        return u"%s/%s" % (self.owner, self.fields['slug'])

    def __repr__(self):
        return "<{0} {1}>".format(self.__class__.__name__, unicode(self))


class MercurialRepo(Repo):
    args = '--time --config extensions.progress= '

    @classmethod
    def from_local_clone(cls, path, manager):
        out, err = exec_command('hg -R {} path | grep default'.format(path))
        line = out.getvalue().split('bitbucket.org/')[1].strip('/\n ')
        repouri = OwnedResource(line)
        manager.owner = repouri.owner
        return manager.get_repo(repouri.name)

    def status(self, path, remote=False):
        remote_arg = '--remote' if remote else ''
        outB, errB = exec_command('hg -R {}/{} summary {}'.format(path, self.name, remote_arg))
        out = outB.getvalue()

        retval = []
        for line in out.splitlines():
            if line.startswith('commit:'):
                retval.append(line)

            if line.startswith('remote:'):
                retval.append(line)
                return retval

        return retval

    def have_local_copy(self, path):
        # FIXME: check this is the same repo
        return os.path.isdir(os.path.join(path, self.name, '.hg'))

    def clone(self, path):
        "Clone repository to the specified destination 'path'"
        exec_command("hg %s clone %s %s/%s" % (self.args, self.uri, path, self.name),
                     self.verbose)
        self.logger.info(u'cloned: %s', self)

    def update(self, path):
        "Update repository in the downloaded in the specified 'path'"
        self.assure_local_copy(path)
        exec_command("hg %s -R %s/%s pull %s -u" % (self.args, path, self.name, self.uri),
                     self.verbose)
        self.logger.info(u'updated: %s', self)


class GitRepo(Repo):
    @classmethod
    def from_local_clone(cls, path, manager):
        # FIXME
        pass

    def status(self, path, remote=False):
        # FIXME
        return []

    def have_local_copy(self, path):
        # FIXME: check this is the same repo
        return os.path.isdir(os.path.join(path, self.name, '.git'))

    def clone(self, path):
        exec_command("git clone %s %s/%s" % (self.uri, path, self.name), self.verbose)
        self.logger.info(u'cloned: %s', self)

    def update(self, path):
        exec_command("git --git-dir %s/%s/.git pull" % (path, self.name), self.verbose)
        self.logger.info(u'updated: %s', self)


class UserOverview:
    def __init__(self, data):
        self.repos = {}
        for info in data:
            self.repos[info['slug']] = info


class RepoManager(object):
    repo_classes = {'hg': MercurialRepo, 'git': GitRepo}

    def __init__(self, session=None, owner=None, proto='http'):
        self.session = session or Session()
        self.owner = owner or self.session.user
        self.proto = proto

        if self.owner is None or '/' in self.owner:
            raise exc.OwnerRequired

        self._overview = None
        self.logger = self.session.logger

    def get_repo_from_local_clone(self, path):
        if os.path.isdir(os.path.join(path, '.hg')):
            scm = 'hg'
        elif os.path.isdir(os.path.join(path, '.git')):
            scm = 'git'
        else:
            scm = None

        return self.repo_classes[scm].from_local_clone(path, self)

    @property
    def overview(self):
        errors = {
            404: [exc.NoSuchUser, self.owner]}

        if self._overview is None:
            self._overview = UserOverview(
                self.session.http_get('user/repositories'.format(self.owner), errors=errors))

        return self._overview

    def invalidate_cache(self):
        self._overview = None

    def get_repo_fields(self, reponame):
        if reponame not in self.overview.repos:
            raise exc.RepositoryNotFound("%s/%s" % (self.owner, reponame))

        return self.overview.repos[reponame]

    def set_repo_field(self, name, key, value):
        self.session.http_put('repositories/%s/%s' % (self.owner, name),
                              data={key: value})
        self.invalidate_cache()

    def get_repo(self, reponame):
        if reponame not in self.overview.repos.keys():
            raise exc.RepositoryNotFound("%s/%s" % (self.owner, reponame))

        scm = self.overview.repos[reponame]['scm']
        return self.repo_classes[scm](reponame, self)

    def get_repos(self, names=None, owner=None):
        names = names or self.list_names(owner)
        return [self.get_repo(x) for x in names]

    def list_names(self, owner=None):
        def filter_owned(repos):
            for k in repos.copy():
                if repos[k]['owner'] != owner:
                    del repos[k]

            return repos

        repos = self.overview.repos
        if owner:
            repos = filter_owned(repos)

        return sorted(repos.keys())

    def create_repo(self, name, scm='hg', private=True, desc=''):
        errors = {
            400: [exc.RepositoryAlreadyExists, name],  # FIXME: incorrect user too
            401: [exc.InvalidCredentials],
            403: [exc.Forbidden]}

        data = dict(owner=self.owner, name=name, scm=scm,
                    is_private=str(bool(private)),
                    description=desc)
        self.session.http_post('repositories', data=data, errors=errors)
        self.invalidate_cache()
        return self.get_repo(name)

    def delete_repo(self, name):
        errors = {
            403: [exc.Forbidden, name],
            404: [exc.RepositoryNotFound, "%s/%s" % (self.owner, name)]}

        data = dict(owner=self.owner)
        self.session.http_delete('repositories/%s/%s' % (self.owner, name),
                                 ok=204, data=data, errors=errors)
        self.invalidate_cache()


class Group(object):
    def __init__(self, session, group_uri):
        self.session = session
        self.group_uri = group_uri

    def get_granted_repos(self, account=None):
        # FIXME: error 404 when group not exists
        account = account or self.group_uri.owner

        uri = 'group-privileges/{0}/{1}'.format(account, self.group_uri)
        return self.session.http_get(uri)
