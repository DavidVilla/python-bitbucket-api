======
bucket
======

-------------------------------
Command line tool for bitbucket
-------------------------------

:Author: David.Villa@uclm.es
:date:   2012-05-05
:Manual section: 1

SYNOPSIS
========

``bucket`` [-h]

DESCRIPTION
===========

This manual page documents briefly the ``bucket`` command.

This manual page was written for the Debian(TM) distribution because
the original program does not have a manual page.

``bucket`` is a program to manage bitbucket.org repositories and user account.


GLOBAL OPTIONS
==============

The global options are:

-a, --account

COMMANDS
========

ls         List user repositories

create     Create a new repository in the bitbucket account

set

clone

sync

del

ssh-ls

ssh-add

ssh-del


COPYRIGHT
=========

Copyright © 2012 David Villa

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU General Public License, Version 2 or (at
your option) any later version published by the Free Software
Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
