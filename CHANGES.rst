0.20130807
==========

- supporting new requests version: session()

0.20130219
==========

- removed "repo-" in bucket commands and config file

0.20121207
==========

- repo_clone probes all account owners
- Using blessings

0.20121011
==========

- Removed RepoMirror. sync() added to Repo.
- command_issue

0.20120524
==========

- --already for repo-sync command.

0.20120523
==========

- --owner argument for repo-sync command.

0.20120522
==========

- Using empty pass accounts force password request on CLI.


.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
