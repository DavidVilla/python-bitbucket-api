# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import sys
sys.path.insert(0, '.')
sys.path.insert(0, 'test')

import shutil
from unittest import TestCase
from nose.plugins.attrib import attr
from nose.tools import nottest

#from pyDoubles.framework import empty_mock, expect_call
from doublex import Mock, verify, assert_that

from bitbucket.api import Session, RepoManager
import bitbucket.exceptions as exc
import bitbucket.api as api

import testutil
config = testutil.DummyConfig('/tmp/dummy.config')

registered_key = 'test/registered.key'
not_registered_key = 'test/not-registered.key'


# class Parser_common_options(TestCase):
#     def test_username_password(self):
#         parser = Parser()
#         parser.parse("--account USER:PASS ls")


# class Parser_SSH_subparser(TestCase):
#     def test_add_key(self):
#         parser = Parser()
#         args = parser.parse("ssh-add LABEL %s" % not_registered_key)
#         self.assert_(hasattr(args, 'keyfile'))


# class ParserTests(TestCase):
#     def test_account(self):
#         argv = "-a john:pasw show-config"
#         config = api.Config()
#         config = Parser().parse(argv, config)


class AccountTests(TestCase):
    def test_ask_password(self):
        with Mock() as mock:
            mock.get_pass('john')

        account = api.Account({}, 'john:', mock.get_pass)

        assert_that(mock, verify())
        self.assertEquals(account.username, 'john')


class FakeConfig(object):
    def __init__(self, user, passw):
        self.username = user
        self.password = passw


class SshKeyManagerTests(TestCase):
    def setUp(self):
        self.session = Session(FakeConfig(config.user, config.pasw))
        self.ssh = self.session.create_ssh_key_manager()
        self.remove_all_keys()

    def remove_all_keys(self):
        for key in self.ssh.list_keys():
            self.ssh.delete_key_by_id(key['pk'])

    def test_list_keys(self):
        self.ssh.add_key('already', file(registered_key))
        keys = self.ssh.list_keys()
        self.assertEquals(len(keys), 1)
        self.assert_(keys[0]['key'].startswith(file(registered_key).read().strip()))

    def test_add_remove(self):
        label = 'drop-key'
        self.assertNotIn(label, self.ssh.list_key_labels())
        pk = self.ssh.add_key(label, file(not_registered_key))
        self.assertIn(label, self.ssh.list_key_labels())
        self.ssh.delete_key_by_id(pk)
        self.assertNotIn(label, self.ssh.list_key_labels())

    def test_add_wrong_key(self):
        with self.assertRaises(exc.InvalidOrAlreadyRegisteredSSHkey):
            self.ssh.add_key('wrong', file(__file__))

    def test_add_already_key(self):
        self.ssh.add_key('already', file(registered_key))
        with self.assertRaises(exc.InvalidOrAlreadyRegisteredSSHkey):
            self.ssh.add_key('already', file(registered_key))


class RepoManagerTests(TestCase):
    def setUp(self):
        self.session = Session(FakeConfig(config.user, config.pasw))
        self.repo_manager = self.session.create_repo_manager()

    def test_owner_is_default_to_user(self):
        self.assertEquals(config.user, self.repo_manager.owner)

    def test_repo_exists(self):
        self.repo_manager.get_repo('hg.public.repo')

    def test_repo_not_exists(self):
        try:
            self.assertFalse(self.repo_manager.get_repo('missing'))
            self.fail()
        except exc.RepositoryNotFound:
            pass

    def test_get_missing_repo(self):
        with self.assertRaises(exc.RepositoryNotFound):
            self.repo_manager.get_repo('missing')

    def test_public_repo_fields_without_credentials(self):
        repo_manager = RepoManager(owner=config.user)
        fields = repo_manager.get_repo_fields('hg.public.repo')
        self.assertEquals(fields['scm'], 'hg')

    def test_private_repo_fields_without_credentials(self):
        repo_manager = RepoManager(owner=config.user)

        with self.assertRaises(exc.RepositoryNotFound):
            repo_manager.get_repo_fields('git.private.repo')

    def test_public_repo_fields_with_credentials(self):
        info = self.repo_manager.get_repo_fields('hg.public.repo')
        self.assertEquals(info['scm'], 'hg')

    def test_private_repo_fields_with_credentials(self):
        info = self.repo_manager.get_repo_fields('git.private.repo')
        self.assertEquals(info['scm'], 'git')

    def test_get_missing_repo_fields(self):
        with self.assertRaises(exc.RepositoryNotFound):
            self.repo_manager.get_repo_fields('missing')

    def test_list_repo_names_with_credentials(self):
        repos_names = self.repo_manager.list_names()
        self.assertItemsEqual(
            repos_names,
            ['hg.public.repo', 'hg.private.repo',
             'git.public.repo', 'git.private.repo'])

    def test_list_repo_names_without_credentials(self):
        repo_manager = RepoManager(owner=config.user)
        repos_names = repo_manager.list_names()
        self.assertItemsEqual(
            repos_names,
            ['hg.public.repo', 'git.public.repo', ])

    def test_create_delete_repo(self):
        repo = 'todrop'
        self.assertFalse(self.repo_manager.repo_exists(repo))
        self.repo_manager.create_repo(repo)
        self.assert_(self.repo_manager.repo_exists(repo))
        self.repo_manager.delete_repo(repo)
        self.assertFalse(self.repo_manager.repo_exists(repo))

    def test_create_existing_repo_is_an_error(self):
        with self.assertRaises(exc.RepositoryAlreadyExists):
            self.repo_manager.create_repo('hg.public.repo')

    def test_delete_nonexisting_repo_is_am_error(self):
        with self.assertRaises(exc.RepositoryNotFound):
            self.repo_manager.delete_repo('missing')


class RepoMirrorTests(TestCase):
    def setUp(self):
        self.path = '/tmp/repos'
        try:
            os.mkdir(self.path)
        except OSError:
            pass

        session = Session(FakeConfig(config.user, config.pasw))
        self.repo_manager = session.create_repo_manager()

    def tearDown(self):
        shutil.rmtree(os.path.join(self.path))

    def repo_is_here(self, repo):
        return repo.have_local_copy(self.path)

    def test_clone_hg_public_repo(self):
        repo = self.repo_manager.get_repo('hg.public.repo')
        repo.clone(self.path)
        self.assert_(self.repo_is_here(repo))

    def test_clone_git_public_repo(self):
        repo = self.repo_manager.get_repo('git.public.repo')
        repo.clone(self.path)
        self.assert_(self.repo_is_here(repo))

    def test_clone_hg_private_repo(self):
        repo = self.repo_manager.get_repo('hg.private.repo')
        repo.clone(self.path)
        self.assert_(self.repo_is_here(repo))

    def test_clone_git_private_repo(self):
        repo = self.repo_manager.get_repo('git.private.repo')
        repo.clone(self.path)
        self.assert_(self.repo_is_here(repo))

    def test_update_hg_repo(self):
        repo = self.repo_manager.get_repo('hg.public.repo')
        repo.clone(self.path)
        repo.update(self.path)

    def test_update_hg_repo_not_here(self):
        repo = self.repo_manager.get_repo('hg.public.repo')
        with self.assertRaises(exc.NotLocalCopy):
            repo.update(self.path)

    def test_update_git_repo(self):
        repo = self.repo_manager.get_repo('git.public.repo')
        repo.clone(self.path)
        repo.update(self.path)

    def test_sync_hg_repo(self):
        repo = self.repo_manager.get_repo('hg.public.repo')
        repo.sync(self.path)
        self.assert_(self.repo_is_here(repo))

    def test_sync_several(self):
        repos = [self.repo_manager.get_repo(x) for x in
                 ['hg.public.repo', 'hg.private.repo', 'git.public.repo']]
        for repo in repos:
            repo.sync(self.path)
        for repo in repos:
            self.assert_(self.repo_is_here(repo))


class RepoTests(TestCase):
    def setUp(self):
        session = Session(FakeConfig(config.user, config.pasw))
        self.repo_manager = session.create_repo_manager()

    def test_get_field(self):
        repo = self.repo_manager.get_repo('hg.public.repo')
        self.assertEquals('hg', repo.fields['scm'])

    def test_get_field_keys(self):
        repo = self.repo_manager.get_repo('hg.public.repo')
        for key in ['scm', 'has_wiki', 'creator', 'logo']:
            self.assertIn(key, repo.fields.keys())

# create with description
# set access
# set description
