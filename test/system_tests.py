# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
sys.path.insert(0, '.')
sys.path.insert(0, 'test')

from unittest import TestCase
import subprocess as sp

from configobj import ConfigObj

import testutil
config = testutil.DummyConfig('/tmp/dummy.config')


def run(cmd):
    return sp.check_output(cmd.split())


class Test_repos_ls(TestCase):
    def assert_list_public_repos(self, out):
        self.assertIn('hg.public.repo', out)
        self.assertIn('git.public.repo', out)

    def assert_list_private_repos(self, out):
        self.assertIn('hg.private.repo', out)
        self.assertIn('git.private.repo', out)

    def test_anon_account_explicit_owner(self):
        out = run('./bin/bucket --config /dev/null ls %s' % config.user)
        self.assert_list_public_repos(out)

    def test_explicit_account_default_owner(self):
        cmd = './bin/bucket --account {0}:{1} ls {0}'.format(
            config.user, config.pasw)
        out = run(cmd)
        self.assert_list_public_repos(out)
        self.assert_list_private_repos(out)

    def test_default_account(self):
        configobj = ConfigObj()
        configobj.filename = '/tmp/bucket.cfg'
        configobj['accounts'] = {'default': "{0}:{1}".format(config.user, config.pasw)}
        configobj.write()

        cmd = './bin/bucket --config {0} ls {1}'.format(
            config.filename, config.user)
        out = run(cmd)
        self.assert_list_public_repos(out)
        self.assert_list_private_repos(out)

    def test_account_by_label(self):
        configobj = ConfigObj()
        configobj.filename = '/tmp/bucket.cfg'
        configobj['accounts'] = {'default': 'JackMay:password',
                                 'jack': "{0}:{1}".format(config.user, config.pasw)}
        configobj.write()

        cmd = './bin/bucket --config {0} --account jack ls'.format(
            configobj.filename)
        out = run(cmd)
        self.assert_list_public_repos(out)
        self.assert_list_private_repos(out)
