#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from hamcrest import contains_string, all_of

from prego import TestCase, Task


class ListingTests(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def assert_public_and_private(self, task, cmd):
        task.assert_that(cmd.stdout.content,
                         all_of(contains_string('git.private.repo'),
                                contains_string('git.public.repo'),
                                contains_string('hg.private.repo'),
                                contains_string('hg.private.repo')))

    def test_explicit_account(self):
        task = Task()
        cmd = task.command('bin/bucket -a arco_test:jijijaja repo-ls')
        self.assert_public_and_private(task, cmd)

    def test_default_account(self):
        task = Task()
        cmd = task.command('bin/bucket repo-ls')
        self.assert_public_and_private(task, cmd)
