# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
sys.path.insert(0, '.')
import io

from unittest import TestCase

from bitbucket.cli import Config

sample_config = '''
foo = bar
'''


class Test0(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_load_from_stringlist(self):
        sut = Config()
        sut.load(sample_config.splitlines())
        self.assertEquals(sut['foo'], 'bar')

    def test_load_from_file(self):
        sut = Config()
        sut.load(io.BytesIO(sample_config))
        self.assertEquals(sut['foo'], 'bar')
